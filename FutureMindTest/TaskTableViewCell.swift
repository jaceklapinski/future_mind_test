//
//  TaskTableViewCell.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import UIKit
import Kingfisher

class TaskTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var modificationDateLabel: UILabel!
    @IBOutlet var taskImageView: UIImageView!

    static let nibName = "TaskTableViewCell"
    static let reuseID = "TaskTableViewCell_ID"

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func update(for task: Tasks) {

        titleLabel.text = task.title
        descriptionLabel.text = task.taskDescriptionForUser

        if let date = task.modificationDate {
            modificationDateLabel.text = "\(date)"
        } else {
            modificationDateLabel.text = "-"
        }

        // here by def we should set image placeholder in case that something goes wrong with cache or download image 

        if let imageUrl = URL(string: task.imageUrl!) {
            taskImageView.kf.indicatorType = .activity
            taskImageView.kf.setImage(with: imageUrl)
        }
    }
}
