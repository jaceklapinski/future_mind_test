//
//  API.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import Foundation
import UIKit

enum APIError {
    case invalidRequestURL
    case requestFailed
}

class API {

    static let shared = API()

    private init() {
    }

    func request(request: URLRequest, onFinish: ((_ response: Data?, _ error: APIError?) -> Void)?) {

        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }

        func onFinishCall(response: Data?, error: APIError?) {

            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                onFinish?(response, error)
            }
        }

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in

            guard let response = response as? HTTPURLResponse else {
                onFinishCall(response: nil, error: .requestFailed)
                return
            }

            guard (200 ..< 300) ~= response.statusCode else {
                onFinishCall(response: nil, error: .requestFailed)
                return
            }

            guard error == nil else {
                onFinishCall(response: nil, error: .requestFailed)
                return
            }

            onFinishCall(response: data, error: nil)
        }

        task.resume()
    }
}
