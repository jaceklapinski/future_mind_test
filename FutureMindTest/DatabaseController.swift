//
//  DatabaseController.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DatabaseController {

    var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }

    static let shared = DatabaseController()

    private init() {

    }

    private func newDBObject(entityName: String) -> NSManagedObject? {

        guard let context = appDelegate?.persistentContainer.viewContext else {
            return nil
        }

        guard let entity = NSEntityDescription.entity(forEntityName: entityName, in: context) else {
            return nil
        }

        return NSManagedObject(entity: entity, insertInto: context)
    }

    func saveDB() {
        appDelegate?.saveContext()
    }
}

// MARK: Tasks
extension DatabaseController {

    func dbNewEntity(from object: ResponseTasks) -> Tasks? {

        guard let taskEntity = newDBObject(entityName: Tasks.entityName)  as? Tasks else {
            return nil
        }

        taskEntity.apply(object: object)

        return taskEntity
    }

    func getTasksList() -> [Tasks] {
        var result: [Tasks] = []

        do {

            let request: NSFetchRequest<Tasks> = Tasks.fetchRequest()
            let sort = NSSortDescriptor(key: "orderId", ascending: true)
            request.sortDescriptors = [sort]

            if let context = appDelegate?.persistentContainer.viewContext {
                result = try context.fetch(request) as [Tasks]
            }

        } catch let error as NSError {
            // Handle error
            print(error)
        }

        return result
    }

    func storeTasks(from array: [ResponseTasks]) -> [Tasks] {

        let currentTasksList = getTasksList()

        var tDict: [Int32: Tasks] = [:]
        for t in currentTasksList {
            tDict[t.orderId] = t
        }

        for t in array {
            if let exitsis = tDict[Int32(t.orderId)] {
                if let dateFromDB = exitsis.modificationDate, let dateFromAPI = t.modificationDateToDate() {
                    if dateFromAPI.timeIntervalSince1970 > dateFromDB.timeIntervalSince1970 { // check date and update only this with modification date from API are > then local task
                        exitsis.apply(object: t)
                    }
                }
            } else {
                _ = self.dbNewEntity(from: t)
            }
        }

        saveDB()
        return getTasksList()
    }
}
