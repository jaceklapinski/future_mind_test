//
//  Tasks+CoreDataClass.swift
//  
//
//  Created by Jacek Łapiński on 30/03/2019.
//
//

import Foundation
import CoreData

@objc(Tasks)
public class Tasks: NSManagedObject {
    static let entityName = "Tasks"

    func apply(object: ResponseTasks) {

        self.orderId = object.orderId
        self.title = object.title
        self.imageUrl = object.image_url
        self.modificationDate = object.modificationDateToDate()

        if let range = object.description.range(of: "\thttp") {
            let urlFromText = object.description[range.lowerBound...]
            let restWithoutUrl = object.description[..<range.lowerBound]

            taskDescriptionForUser = String(restWithoutUrl)
            urlToTap = String(urlFromText.trimmingCharacters(in: .whitespaces))
        }
    }
}
