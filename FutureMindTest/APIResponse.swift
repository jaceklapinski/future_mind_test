//
//  APIResponse.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import Foundation

class APIResponse<T: Decodable> {

    var jsonData: Data?
    var object: T?
    var noContent = false

    init(for type: T.Type, responseData: Data?) {
        self.jsonData = responseData
        self.noContent = false
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970

        if let jData = jsonData {
            do {
                object = try decoder.decode(T.self, from: jData)
            } catch {
                object = nil
            }
        }
    }

    init(noContentResponse: Bool) {
        self.noContent = noContentResponse
    }
}
