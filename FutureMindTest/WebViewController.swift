//
//  WebViewController.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet var webView: WKWebView!

    var urlToShow: String?
    var taskTitle: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        webView.navigationDelegate = self

        title = taskTitle

        if let url = URL(string: urlToShow ?? "") {
            let request = URLRequest(url: url)
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            webView.load(request)
        } else {
            let alert = UIAlertController(title: Strings.warning, message: Strings.invalidURL, preferredStyle: .alert)
            let okAction = UIAlertAction(title: Strings.ok, style: .default, handler: nil)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    }
}

extension WebViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
