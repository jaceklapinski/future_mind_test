//
//  ViewControllers.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import Foundation

class ViewControllers {
    static let mainViewController = "MainViewController"
    static let waitHudViewController = "WaitHudViewController"
    static let webViewController = "WebViewController"
}
