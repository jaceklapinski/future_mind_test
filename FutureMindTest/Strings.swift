//
//  Strings.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import Foundation

class Strings {
    static let taskList = NSLocalizedString("Tasks list", comment: "")
    static let dataUpdateInProgress = NSLocalizedString("Data update in progress ...", comment: "")
    static let warning = NSLocalizedString("Warning", comment: "")
    static let ok = NSLocalizedString("OK", comment: "")
    static let noInternetConnection = NSLocalizedString("Internet connection is required to perform data update", comment: "")
    static let invalidURL = NSLocalizedString("Can't show content. Url is invalid", comment: "")
}
