//
//  Tasks+CoreDataProperties.swift
//  
//
//  Created by Jacek Łapiński on 30/03/2019.
//
//

import Foundation
import CoreData

extension Tasks {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tasks> {
        return NSFetchRequest<Tasks>(entityName: "Tasks")
    }

    @NSManaged public var title: String?
    @NSManaged public var orderId: Int32
    @NSManaged public var imageUrl: String?
    @NSManaged public var taskDescription: String?
    @NSManaged public var modificationDate: NSDate?
}
