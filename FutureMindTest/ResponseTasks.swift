//
//  ResponseTasks.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import Foundation

struct ResponseTasks: Decodable {

    let orderId: Int32
    let title: String
    let description: String
    let image_url: String
    let modificationDate: String

    func modificationDateToDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: modificationDate)
    }
}
