//
//  ViewController.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet var refreshButton: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!

    private var taskList: [Tasks] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        title = Strings.taskList

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = view.frame.height * 0.2

        tableView.register(UINib(nibName: TaskTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: TaskTableViewCell.reuseID)
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self

        taskList = DatabaseController.shared.getTasksList()
        tableView.reloadData()

        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { timer in // Better UX not start action when app is not complete show
            timer.invalidate()
            self.dataUpdate()
        }
    }

    @IBAction func refreshTap(_ sender: Any) {
        dataUpdate(refreshFromButton: true)
    }

    private func dataUpdate(refreshFromButton: Bool = false) {

        func showCurrentTasksFromDB() {
            self.taskList = DatabaseController.shared.getTasksList()
            self.tableView.reloadData()
        }

        guard Reachability.isConnectedToNetwork() else {
            showCurrentTasksFromDB()
            let alert = UIAlertController(title: Strings.warning, message: Strings.noInternetConnection, preferredStyle: .alert)
            let okAction = UIAlertAction(title: Strings.ok, style: .default, handler: nil)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            return
        }

        guard let urlFromString = URL(string: "https://www.futuremind.com/recruitment-task") else {
            showCurrentTasksFromDB()
            return
        }

        let request = URLRequest(url: urlFromString)

        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewControllers.waitHudViewController) else {
            return
        }

        vc.modalPresentationStyle = .overCurrentContext

        self.present(vc, animated: true) {
            API.shared.request(request: request) { (responseData, error) in
                vc.dismiss(animated: true, completion: {

                    guard error == nil else {
                        // Handle error ( dialog for user for example )
                        showCurrentTasksFromDB()
                        return
                    }

                    let response = APIResponse(for: [ResponseTasks].self, responseData: responseData)

                    guard let responseData = response.object else {
                        // Handle mapping error
                        showCurrentTasksFromDB()
                        return
                    }

                    self.taskList = DatabaseController.shared.storeTasks(from: responseData)
                    self.tableView.reloadData()
                })
            }
        }
    }
}

// MARK: UITableViewDataSource
extension MainViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: TaskTableViewCell.reuseID, for: indexPath) as? TaskTableViewCell else {
            return UITableViewCell()
        }

        cell.update(for: taskList[indexPath.row])

        return cell
    }
}

// MARK: UITableViewDelegate
extension MainViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        guard let webViewVc = storyboard?.instantiateViewController(withIdentifier: ViewControllers.webViewController) as? WebViewController else {
            return
        }

        webViewVc.urlToShow = taskList[indexPath.row].urlToTap
        webViewVc.taskTitle = taskList[indexPath.row].title

        navigationController?.pushViewController(webViewVc, animated: true)
    }
}
