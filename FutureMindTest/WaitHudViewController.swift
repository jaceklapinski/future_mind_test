//
//  WaitHudViewController.swift
//  FutureMindTest
//
//  Created by Jacek Łapiński on 30/03/2019.
//  Copyright © 2019 Jacek Łapiński. All rights reserved.
//

import UIKit

class WaitHudViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var mainView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.layer.cornerRadius = 5.0
        mainView.layer.shadowRadius = 2.0
        mainView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        mainView.layer.shadowColor = UIColor.black.cgColor
        mainView.layer.shadowOpacity = 2.0

        titleLabel.text = Strings.dataUpdateInProgress

        activityIndicator.startAnimating()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        activityIndicator.stopAnimating()
    }
}
